#!/usr/bin/make -f
include Makefile.msg

help:
	$(call amsg,Available targets are:)
	$(call amsg,)
	$(call amsg,- install)
	$(call amsg,- lint)

install:
	$(call bmsg,Installing poetry and dependencies.)
	$(call qcmd,pip install -U poetry==1.7.1)
	$(call qcmd,poetry install)

lint:
	$(call bcmd,pre-commit,run,-poetry run pre-commit run --all-files)

clean:
	$(call rmsg,Cleaning build and cache directories.)
	$(call qcmd,rm -rf build .coverage htmlcov .mypy_cache .pytest_cache docs/_build)

.PHONY: help install lint clean
