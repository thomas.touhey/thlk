function copyShorten() {
	const text = document.getElementById('linktext');
	const button = document.getElementById('copybutton');
	const arbitraryTextInput = document.createElement('textarea');

	arbitraryTextInput.value = text.value;
	document.body.appendChild(arbitraryTextInput);
	arbitraryTextInput.select();
	arbitraryTextInput.setSelectionRange(0, 99999);
	document.execCommand('copy');
	document.body.removeChild(arbitraryTextInput);

	button.innerHTML = 'Copied!';
	button.style.color = '#008000';
}
