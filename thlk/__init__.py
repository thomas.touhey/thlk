#!/usr/bin/env python3
"""Small, very basic shortlinker."""

from __future__ import annotations

import typing as t
from os import environ
from os.path import join as joinpaths
from secrets import token_urlsafe
from time import sleep

from dotenv import load_dotenv
from flask import Flask, redirect, render_template, request, send_file
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFError, CSRFProtect
from sqlalchemy.exc import IntegrityError, OperationalError
from validators.url import url as url_validator

__all__ = ['app']

load_dotenv(verbose=False)

# ---
# Globals.
# ---

app = Flask('thlk')
app.config.update({
    'SECRET_KEY': environ['SECRET_KEY'],
    'SQLALCHEMY_DATABASE_URI': environ['DATABASE_URI'] or None,
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
})

db = SQLAlchemy(app)

csrf = CSRFProtect()
csrf.init_app(app)

# ---
# Database models.
# ---


class Link(db.Model):  # type: ignore
    __tablename__ = 'links'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(12), nullable=False, unique=True)
    url = db.Column(db.String(1000), nullable=False)


# ---
# Utilities.
# ---


class LinkNotFoundError(Exception):
    """Link not found by name."""

    def __init__(self, name: str):
        super().__init__(f'link {name!r} was not found by name')
        self._name = name

    @property
    def name(self) -> str:
        """Name of the link that wasn't found."""
        return self._name


class InvalidURLError(Exception):
    """Invalid URL error."""

    def __init__(self, url: str, desc: str):
        super().__init__(f'url {url!r} was invalid: {desc}')
        self._url = url
        self._desc = desc

    @property
    def url(self) -> str:
        """Get the URL that couldn't be validated."""
        return self._url

    @property
    def description(self) -> str:
        """Get the description of the error."""
        return self._desc


def get_link(name: str) -> str:
    """Get the link corresponding to a link."""
    link = db.session.query(Link).filter((Link.name == name)).first()
    if link is None:
        raise LinkNotFoundError(name)

    return link.url


def add_link(url: str) -> str:
    """Make a link, and return the name.

    Note that a link MAY have different names in the database due to
    race conditions!
    """
    if type(url) is not str:
        raise InvalidURLError(url, 'missing URL')
    if len(url) > 1000:
        raise InvalidURLError(url, 'longer than 1000 chars')
    if not url.startswith('http:') and not url.startswith('https:'):
        raise InvalidURLError(url, 'not an http or https url')

    result = url_validator(url, public=True)
    if not result:
        raise InvalidURLError(url, 'invalid URL')

    # First, check if we need to recreate the link.
    # Let's not have duplicates.

    link = db.session.query(Link).filter((Link.url == url)).first()
    if link is not None:
        return link.name

    # The link doesn't exist in the database!
    # Let's generate

    while True:
        token = token_urlsafe(6)

        try:
            link = Link(name=token, url=url)
            db.session.add(link)
            db.session.commit()
        except IntegrityError:
            # Probably a unique constraint violation... let's
            # try and generate another token.

            continue

        return link.name


# ---
# Web application bindings.
# ---


@app.route('/favicon.ico')
def _faviconico() -> t.Any:
    return send_file(joinpaths(app.root_path, 'resources', 'favicon.ico'))


@app.route('/favicon.png')
def _faviconpng() -> t.Any:
    return send_file(joinpaths(app.root_path, 'resources', 'favicon.png'))


@app.route('/robots.txt')
def _robotstxt() -> t.Any:
    return send_file(joinpaths(app.root_path, 'resources', 'robots.txt'))


@app.route('/', methods=['GET'])
def _getindex() -> t.Any:
    """Get the main page."""
    return render_template('default.html')


@app.route('/', methods=['PUT', 'POST'])
def _addlink() -> t.Any:
    """Add a link."""
    name = add_link(request.form.get('url'))
    return render_template('default.html', name=name)


@app.route('/<name>')
def _getlink(name: str) -> t.Any:
    """Get the link."""
    return redirect(get_link(name))


@app.errorhandler(InvalidURLError)
def _invalidurl(error: InvalidURLError) -> t.Any:
    """Get the error specifying an invalid URL has been given."""
    return render_template(
        'default.html',
        error=f'An invalid URL has been given: {error.description}',
    ), 400


@app.errorhandler(LinkNotFoundError)
def _linknotfound(error: LinkNotFoundError) -> t.Any:
    """Get the error specifying a link wasn't found."""
    return render_template(
        'default.html',
        error="This shortlink doesn't exist.",
    ), 404


@app.errorhandler(CSRFError)
def _csrferror(error: CSRFError) -> t.Any:
    """Get the error specifying the CSRF header was invalid or missing."""
    return render_template('default.html', error=error.description), 400


# ---
# Command-line interface.
# ---


@app.cli.command('init-db')
def init_db() -> None:
    """Create the database."""
    while True:
        try:
            db.create_all()
        except OperationalError:
            sleep(.25)
            continue

        break


@app.cli.command('gen-key')
def gen_key() -> None:
    """Generate a secret key."""
    print(token_urlsafe(40))


if __name__ == '__main__':
    app.cli()
