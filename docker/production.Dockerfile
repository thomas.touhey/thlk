FROM alpine:3.18 as builder

WORKDIR /opt/thlk

RUN \
    apk add --no-cache python3=3.11.6-r0 python3-dev=3.11.6-r0 \
        py3-pip=23.1.2-r0 alpine-sdk=1.0-r1 cmake=3.26.5-r0 \
        linux-headers=6.3-r0 postgresql15-dev=15.5-r0 \
    && pip3 --no-cache-dir install poetry==1.7.1

COPY ./pyproject.toml .
COPY ./poetry.lock .

RUN \
    poetry export --with deployment --format requirements.txt \
        --output requirements.txt \
    && python -m venv /opt/thlk/venv \
    && /opt/thlk/venv/bin/python -m pip install -r requirements.txt

FROM alpine:3.18 as base
ENV PYTHONUNBUFFERED=1, PYTHONDONTWRITEBYTECODE=1, FLASK_APP=thlk

WORKDIR /opt/thlk
RUN apk add --no-cache python3=3.11.6-r0 postgresql15=15.5-r0

COPY ./thlk ./thlk
COPY --from=builder /opt/thlk/venv ./venv

FROM base as create-db
ENTRYPOINT ["/opt/thlk/venv/bin/python", "-m", "flask", "init-db"]

FROM base as wsgi
EXPOSE 80
ENTRYPOINT ["/opt/thlk/venv/bin/python", "-m", "gunicorn", "thlk:app", "-w", "1", "--bind", "0.0.0.0:80"]
